var APP_DATA = {
  "scenes": [
    {
      "id": "0-hall",
      "name": "Hall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.4717183126371403,
        "pitch": 0.021650094501579176,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 1.5162671594362749,
          "pitch": 0.1821767654958002,
          "rotation": 0,
          "target": "1-patio"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-patio",
      "name": "Patio",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -3.0626305488848065,
        "pitch": 0.022774205541695736,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": 0.06861174370716228,
          "pitch": 0.28725373549209365,
          "rotation": 0,
          "target": "0-hall"
        },
        {
          "yaw": 1.6343376923399697,
          "pitch": 0.12757506573509936,
          "rotation": 10.995574287564278,
          "target": "5-sala-electricidad"
        },
        {
          "yaw": -1.3805496768522438,
          "pitch": 0.17658526946986086,
          "rotation": 1.5707963267948966,
          "target": "4-sala-noria-y-aparejos"
        },
        {
          "yaw": -2.1881828725090067,
          "pitch": 0.1398208925069948,
          "rotation": 7.0685834705770345,
          "target": "2-patio-derecha"
        },
        {
          "yaw": 2.332624630589776,
          "pitch": 0.13881249251702954,
          "rotation": 5.497787143782138,
          "target": "3-patio-izquierda"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-patio-derecha",
      "name": "Patio (Derecha)",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 3.0714691338760876,
          "pitch": 0.09675944865892205,
          "rotation": 0,
          "target": "1-patio"
        },
        {
          "yaw": -0.5768180153019653,
          "pitch": 0.14417368249246998,
          "rotation": 0,
          "target": "6-sala-recursos"
        },
        {
          "yaw": 0.6523252218381224,
          "pitch": 0.16781206449335428,
          "rotation": 0,
          "target": "9-antesala-taller-de-reciclado"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-patio-izquierda",
      "name": "Patio (Izquierda)",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.7330582075043388,
        "pitch": 0.07965256427793577,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": -1.476424576231473,
          "pitch": 0.2046732350990368,
          "rotation": 0,
          "target": "1-patio"
        },
        {
          "yaw": -0.062096302023427086,
          "pitch": 0.24956108451214476,
          "rotation": 0,
          "target": "7-sala-ptica"
        },
        {
          "yaw": 1.6113798138028512,
          "pitch": 0.2842595684330824,
          "rotation": 0,
          "target": "11-pasillo"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-sala-noria-y-aparejos",
      "name": "Sala: Noria Y Aparejos",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -2.0802531920049248,
        "pitch": 0.1361122365729237,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 1.2381510422468356,
          "pitch": 0.1850932647948209,
          "rotation": 0,
          "target": "1-patio"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.1635409630673017,
          "pitch": 0.26303147698400586,
          "title": "Poleas y Aparejos",
          "text": "La interacción con poleas y aparejos para levantar bolsas de arena nos da la oportunidad de conversar sobre los conceptos de fuerza, trabajo, transmisión de energía y sus aplicaciones. En la actualidad se pueden encontrar estos dispositivos en muchos mecanismos que usamos cotidianamente."
        },
        {
          "yaw": -3.1232507884057767,
          "pitch": 0.06170080721054916,
          "title": "Noria Humana",
          "text": "Las norias se usaban para sistemas de molienda, elevación de agua y telares. En este módulo interactivo se trabajan las distintas formas de energía; cinética, potencial, eléctrica, lumínica, calórica, son algunas de ellas. El objetivo es discutir los conceptos de fuerza, transmisión (o transformación) de energía, sus distintas expresiones y sus aplicaciones."
        },
        {
          "yaw": 1.7930742081617925,
          "pitch": 0.26889647233243075,
          "title": "Levitador de Bernoulli",
          "text": "Una de las leyes fundamentales que explican el movimiento de los fluidos es el teorema de Bernoulli. Este resultado relaciona un aumento en la velocidad de flujo con una disminución de la presión. Durante la exploración de este módulo interactivo descubriremos cómo esta relación se vincula con el vuelo de los aviones."
        }
      ]
    },
    {
      "id": "5-sala-electricidad",
      "name": "Sala: Electricidad",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -0.9574707718040791,
        "pitch": 0.12786786802355365,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 3.0395414151181592,
          "pitch": 0.15460670233264828,
          "rotation": 0,
          "target": "1-patio"
        },
        {
          "yaw": -2.2253776859196126,
          "pitch": 0.08087006345652625,
          "rotation": 0,
          "target": "8-caleidoscopio"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.9376090650118467,
          "pitch": -0.10218907203621974,
          "title": "Caleidoscopio",
          "text": "En nuestra versión ampliada del caleidoscopio tradicional, podés ver tu imagen reflejada tantas veces como puedas contar. Este caleidoscopio gigante consta de tres espejos planos que forman un triángulo equilátero y su objetivo es comprender fenómenos relacionados con la reflexión de la luz. Nos meteremos adentro a ver qué pasa, observaremos y contaremos nuestras propias imágenes tratando de explicar esta extraña multiplicación."
        },
        {
          "yaw": -0.19566352053599623,
          "pitch": 0.5895647845618477,
          "title": "Esfera de plasma",
          "text": "La interacción con una esfera de plasma nos permite conversar sobre uno de los estados de la materia menos conocidos. Si además la cubrimos con papel de aluminio o le acercamos un tubo fluorescente la experiencia se enriquece aún más."
        },
        {
          "yaw": 1.3256085091587977,
          "pitch": 0.1186310130687449,
          "title": "Generador de Van der Graaf",
          "text": "En la sala donde se encuentra el generador de Van der Graaf les visitantes experimentarán y conversaremos sobre los conceptos de carga eléctrica, electroestática y carga de materiales por fricción reconociendo estos fenómenos en la vida cotidiana.&nbsp;"
        }
      ]
    },
    {
      "id": "6-sala-recursos",
      "name": "Sala: Recursos",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 1.0592805998642572,
        "pitch": 0.2148766850288304,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": -2.3018490942793317,
          "pitch": 0.10771664568995831,
          "rotation": 0.7853981633974483,
          "target": "2-patio-derecha"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.788083548430869,
          "pitch": -0.026497980656968068,
          "title": "Huella Hídrica",
          "text": "Todos los objetos de la vida cotidiana tienen relación con el consumo de agua. A partir de esta premisa les visitantes ubicarán algunos elementos bajo una escala creciente de litros consumidos. Esta acción es disparadora de la reflexión, del acercamiento al concepto de huella hídrica y a la relación entre consumo e impacto ambiental."
        },
        {
          "yaw": 1.6557095263549417,
          "pitch": 0.444081283582932,
          "title": "Compost",
          "text": "Una observación detallada, directa o con instrumentos como lupas, permite la indagación sobre el concepto de suelo. Los organismos que viven en la tierra invitan a repensar la importancia de los insectos, del compost y de la agricultura libre de tóxicos. Tomar contacto con la tierra es una invitación a valorarla."
        },
        {
          "yaw": 0.03717188922495751,
          "pitch": 0.3296758193777336,
          "title": "Juego de las energías",
          "text": "Este juego de mesa presenta la posibilidad de conocer la matriz energética nacional. A partir de allí se dará lugar a la reflexión acerca de las fuentes de energía, de la dependencia energética de fuentes no renovable, las ventajas y desventajas de las llamadas “energías limpias”. Hipotetizar modelos alternativos evaluando el impacto ambiental es uno de los propósitos."
        }
      ]
    },
    {
      "id": "7-sala-ptica",
      "name": "Sala: Óptica",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.2944560658311026,
        "pitch": 0.086346776321232,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 1.584269341410268,
          "pitch": 0.14531275967224566,
          "rotation": 0,
          "target": "3-patio-izquierda"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.1896508926130558,
          "pitch": 0.19215126355308776,
          "title": "Espejos Cóncavo y Convexo",
          "text": "Muchos de nosotres alguna vez nos hemos visto reflejados en los lados de una cuchara de metal o en una pava. Los espejos convexos y los cóncavos también reflejan tu imagen, pero tal vez te veas de cabeza, más pequeño o más grande. Vení a descubrir qué sucede con nuestras imágenes corporales en estos espejos y su relación con las comunicaciones satelitales, sistemas de seguridad y la astronomía.&nbsp;"
        },
        {
          "yaw": -3.03644939149871,
          "pitch": 0.21923480803703121,
          "title": "Mezclador de caras&nbsp;",
          "text": "En este dispositivo se combinan la refracción y reflexión de la luz. Se trata de un vidrio con una lámina de \"polarizado\", a cada lado del vidrio se coloca un foco al que se le puede graduar la intensidad de la luz que emite. Lo que ocurre es que al sentarse una persona de cada lado del \"espejo\" ambos ven su cara \"mezclada\" con la del otro. Lo interesante es ir probando cómo cambia la imagen que cada uno ve dependiendo de la intensidad de la luz de cada lado."
        },
        {
          "yaw": 2.2820378010652203,
          "pitch": 0.14857292685614532,
          "title": "Holograma",
          "text": "Uno de los sistemas para observar los hologramas es el sistema holográfico a 4 caras, que se conoce como pirámide holográfica, se trata de 4 proyecciones que muestran el objeto en cuestión que con un prisma piramidal se reúnen y forman una única imagen en 3D. Podemos observar la imagen flotando en medio del prisma con un efecto de 360º."
        },
        {
          "yaw": 1.218767739531863,
          "pitch": 0.20882052968933706,
          "title": "Luces de colores",
          "text": "Este dispositivo permite experimentar con luces de colores y sus mezclas. Veremos que la luz blanca está formada por todos los colores. Normalmente vemos nuestra sombra y la de todos los objetos color negra, esto es así porque en general estamos iluminados por luz del sol o luz artificial, blanca en ambos casos. Esto cambia cuando experimentamos con las tres lámparas de color de este dispositivo."
        },
        {
          "yaw": 0.9540189940526638,
          "pitch": 0.022090328049493735,
          "title": "Túnel de Luces",
          "text": "En este módulo las imágenes de las lámparas se multiplican generando una apariencia de túnel, recurso usado muchas veces en el cine. Intentaremos desentrañar el misterio y explicar, desde la óptica, el fenómeno observado."
        }
      ]
    },
    {
      "id": "8-caleidoscopio",
      "name": "Caleidoscopio",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.4256306927012492,
        "pitch": -0.010997644193391665,
        "fov": 1.4223619823453562
      },
      "linkHotspots": [
        {
          "yaw": -1.5590100582687185,
          "pitch": 0.02936002281627026,
          "rotation": 3.141592653589793,
          "target": "5-sala-electricidad"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-antesala-taller-de-reciclado",
      "name": "Antesala: Taller De Reciclado",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 2.890630082216428,
        "pitch": 0.08997299619431232,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 2.7363252036647783,
          "pitch": 0.11772104648550297,
          "rotation": 0,
          "target": "10-taller-de-reciclado"
        },
        {
          "yaw": -1.5451096584772301,
          "pitch": 0.2317754208317453,
          "rotation": 0,
          "target": "2-patio-derecha"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-taller-de-reciclado",
      "name": "Taller De Reciclado",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.9203460783077624,
        "pitch": 0.06946445775019505,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 0.1496280068464486,
          "pitch": 0.21636139579301705,
          "rotation": 0,
          "target": "9-antesala-taller-de-reciclado"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-pasillo",
      "name": "Pasillo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": -1.6747953446558856,
        "pitch": -0.003520499332736904,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": 1.735131726493524,
          "pitch": 0.18200569736948324,
          "rotation": 11.780972450961727,
          "target": "3-patio-izquierda"
        },
        {
          "yaw": 3.0500780140624943,
          "pitch": 0.18117014088896433,
          "rotation": 0,
          "target": "12-mi-pequea-ciudad"
        },
        {
          "yaw": -0.10618693917620803,
          "pitch": 0.2651958127688001,
          "rotation": 0,
          "target": "13-del-otro-lado-del-muro"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-mi-pequea-ciudad",
      "name": "Mi Pequeña Ciudad",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "yaw": 0.3990225978384476,
        "pitch": 0.25704383515934737,
        "fov": 1.3874365266085906
      },
      "linkHotspots": [
        {
          "yaw": -2.385104145438193,
          "pitch": 0.17000014420516862,
          "rotation": 5.497787143782138,
          "target": "11-pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.49833400594142674,
          "pitch": 0.11299961042985629,
          "title": "Mi pequeña ciudad",
          "text": "La exploración lúdica del espacio recreado como una ciudad posibilita vivenciar la experiencia de la ciudadanía. Les visitantes recorrerán tres rincones (hospital, escuela y supermercado), además de una plaza en el centro y un colectivo que puede circular. Les niñes identificarán en la conversación los ámbitos públicos y privados, las instituciones y la idea de derecho a la ciudad."
        }
      ]
    },
    {
      "id": "13-del-otro-lado-del-muro",
      "name": "Del Otro Lado Del Muro",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        }
      ],
      "faceSize": 752,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.093661958866459,
          "pitch": 0.22529027662367973,
          "rotation": 0.7853981633974483,
          "target": "11-pasillo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.024421509034439737,
          "pitch": 0.09296767273568562,
          "title": "Del otro lado del muro",
          "text": "Mediante un juego de roles se abordarán problemáticas referidas al acceso al hábitat y las disputas entre diferentes actores en el territorio. El escenario lúdico planteará situaciones y posiciones que encarnarán les visitantes con el fin de resolver ciertos problemas. Los muros, físicos y simbólicos, son una oportunidad para repensar derechos y prejuicios."
        }
      ]
    }
  ],
  "name": "Recorrido: Museo Imaginario",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": true,
    "viewControlButtons": false
  }
};
